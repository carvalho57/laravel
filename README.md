# run
docker-compose build && docker-compose up -d

# Commands

## create new laravel project
laravel new project-name
composer create-project laravel/laravel project-name

## composer
composer init 
composer install 
composer update 
    > search for lockfile in the current directory and downloads package and create lockfile
composer search package-name > search for a package like name
composer show --tree > list dependencies
composer dump-autoload 
    > regenerate the list of all classes that need to be included in the autoloader
if this config option ""optimize-autoloader" is true will generate optimized autoload


## artisan
php artisan serve
php artisan serve --port number
php artisan make:controller

php artisan storage:link 
    > create a link between the specified directory and the public
    > security reasons (search for)

# Documentation
https://laravel.com/docs/9.x

# Datase

## Migrations
php artisan make:migration create_posts_table
### Create a migration when create/update a model
php artisan make:migration create_posts_table -m

php artisan migrate

php artisan migrate:install
php artisan migrate:reset
    - rollback all migration
php artisan migrate:refresh
    - reset and re-reun all mgirations
php artisan migrate:rollback
    - rollback the last one
php artisan migrate:status

### Factory
php artisan make:factory name-of

php artisan tinker

\App\Models\Post::factory()->count(numberofrows)create();


### Eloquent

is a ORM, its a database abstract layer that provides a single interface to interact to multiple databases types.

Car interact for default with the table cars

snakecase and pluralize you class name

## Route
php artisan route:list

## Other Commands

php artisan clear-compiled
php artisan down - put the application into maintenance mode
php artisan up - Bring the application out of maintenance mode
php artisan optimize - cache the framework bootstrap files

## Auth

php artisan ui tailwindcss --auth

## Validation

php artisan make:rule Uppercase

´´´
$request->validate([
            'name' => 'required|unique:cars',
            'founded' => 'required|integer|min:0|max:2022',
            'description' => 'required'
        ]);
        
´´´

