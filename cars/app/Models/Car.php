<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    protected $table = 'cars';
    protected $primaryKey = 'id';
    // protected $timestamps = true;
    // protected $dataFormat = 'h:m:s';

    protected $fillable = ['name', 'founded', 'description', 'image_path', 'user_id'];
    // protected $hidden = ['updated_at']; //hidden specif properties
    protected $visible = ['name', 'founded', 'description'];

    //A car has many models
    public function carModels() {
        return $this->hasMany(CarModel::class);
    }

    //Define a has many  relationship
    public function engines() {
        return $this->hasManyThrough(
            Engine::class, 
            CarModel::class,
            'car_id', //Foreign key on CarModel table
            'model_id'//Foreign key on Engine table
        );
    }

    //Defina a has one through relationship
    public function productionDate() {
        return $this->hasOneThrough(
            CarProductionDate::class,
            CarModel::class,
            'car_id',
            'model_id'
        );
    }

    public function products() {
        return $this->belongsToMany(Product::class);
    }
}
