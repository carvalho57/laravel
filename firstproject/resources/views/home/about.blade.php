@if (10 < 100)
    <p> 10 is lower than 100</p>
@else
    <p> All conditions are false</p>
@endif

{{-- Works like a inverse if -> if(!empty($name)) --}}
@unless (empty($name))
    <p>Name isn't empty</p>
@endunless

@empty($name) 
    <p> Name is empty </p>
@endempty

@isset($name)
    <p>Name has been set</p>
@endisset


<!---
    For loop 
    Foreach loop
    Forelse loop
    while loop
-->

@for ($i = 0; $i < 10; $i++) 
    <p>The number is: {{ $i }}</p>
@endfor


<h2>Estados: </h2>

@foreach ($states as $uf => $state)
    <p>{{ $uf }} - {{ $state }}</p>
@endforeach

<h2>Values</h2>

{{-- The same as foreach, but verify if its empty --}}
@forelse ($names as $name)
    <p>The name is: {{ $name }}</p>
@empty
    <p> There are no names</p>
@endforelse