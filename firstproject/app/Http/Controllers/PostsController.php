<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function index()  {

        //Non fluent
        // $post = DB::select('select * from posts where id = ?', [1]);
        
        //Fluent
        // DB::table('posts')->where('id',1)->get();

        $posts = DB::table('posts')->get();        
        // dd($posts);    

        $post = DB::select('select * from posts where id = :id', [':id' => 1]);
        

        return view('posts.index')
            ->with('posts', $posts)
            ->with('post', $post);
    }

    public function show() {
        $id = 1;

        // $posts = DB::table('posts')
        //     ->where('created_at', '>', now())
        //     ->orWhere('title','Mr.')
        //     ->get();

        // $posts = DB::table('posts')
        //         ->whereBetween('id',[1,3])
        //         ->get();

        // $posts = DB::table('posts')
        //         ->select('title')
        //         ->distinct()
        //         ->get();
        
        $posts = DB::table('posts')
                ->orderByDesc('title')
                ->get();

        /* Without using get, will always return the query builder instance
            Other returns methods

            find
            first
            count
            min(field)
            max(field)
            sum(field) 
            avg(field)
        */

        DB::table('posts')
            ->insert([
                'title' => 'New Title',
                'body' => 'New Body'
            ]);

        /*
            DB:table('posts')->where()->update
            DB:table('posts')->where()->delete
         */

        dd($posts);
    }
}
