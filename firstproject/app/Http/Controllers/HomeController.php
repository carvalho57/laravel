<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index() {
        return view('home.index');
    }

    public function about() {
        $name = 'Gabriel';

        $states = [
            'PR' => 'Paraná',
            'SC' => 'Santa Catarina',
            'RJ' => 'Rio de janeiro',
            'SP' => 'São Paulo'
        ];

        return view('home.about')
            ->with('name', $name)
            ->with('states', $states)
            ->with('names', []);
    }
}
